<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>LaravelPH</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
		background-color: #123;
		color: #fefefe;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

	    .color-white { 
               color: white !important; 
            } 
            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }
	    .links { padding-top: 10%; } 
            .links > a {
                padding: 50px 25px;
                font-size: 2.5rem;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
	    }
		a, a:Visited, a:Link, a:Active { color: white; }
            .m-b-md {
                margin-bottom: 30px;
	    }

	    .header { 
		position:relative;
		top: 1px;
                height: 130px;
		width: 100vw;
		background: orange url("Header-New.png") center;
		background-repeat: no-repeat;
		background-color: orange;
	
	    }

.header h1 { 
padding-top: 25px;
margin-left: 15px;
color: white;
}

.card {
  height: 19rem;
  width: 15rem;
}

.card > p {

  top: 3rem; 
}

.meetup-card > h2 { 
background: #fc4a1a; /* fallback for old browsers */
  background: -webkit-linear-gradient(to right, #fc4a1a, #f7b733); /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to right, #fc4a1a, #f7b733); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
  font-size: 2rem;
  padding-top: 1.8rem;
}
.card.meetup-card { 

background: #fefefe !important; /* fallback for old browsers */



}
        </style>
    </head>
    <body>
<div class="header">
   <h1>LaravelPH</h1>
</div>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">

                <div class="links color-white" >
                   <a href="http://facebook.com/laravelph">Join us</a>
		</div>
		<div class="card meetup-card" style="background:white;color:black;">
			<h2>Meetups</h2>
			<div class="card-content">
				<p> Coming soon </p>
			<div>
		</div>
            </div>
        </div>
    </body>
</html>
